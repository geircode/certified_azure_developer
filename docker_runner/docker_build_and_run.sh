#!/bin/sh

cd /app
docker build -f Dockerfile -t geircode/azuredeveloper_ef3a8e3 .
docker-compose -f docker-compose.yml up -d --build --remove-orphans
docker exec -it azuredeveloper_ef3a8e3-1 /bin/bash
