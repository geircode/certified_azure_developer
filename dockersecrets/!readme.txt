# Docker Secrets setup

Script container for setting up Secrets for this repository

## Usage

Start Container by running: *docker-compose.dockersecrets.up.bat*

```shell
ssh-keygen -t rsa -b 4096 -C your@email.no -P MyPassword -f /azuredeveloper_ef3a8e3_secrets/ubuntu_rsa
```

On Windows Host, navigate to "..\..\DockerSecrets\azuredeveloper_ef3a8e3_secrets" to find the SSH Keys.
