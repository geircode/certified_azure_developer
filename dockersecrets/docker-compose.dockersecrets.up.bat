cd %~dp0
start cmd /C MakeDockerSecretsFolder.bat
docker rm -f azuredeveloper_ef3a8e3_secrets-1
docker-compose -f docker-compose.dockersecrets.yml down --remove-orphans
docker-compose -f docker-compose.dockersecrets.yml up --build --remove-orphans
pause
start "" ..\..\DockerSecrets\azuredeveloper_ef3a8e3_secrets
