# FROM geircode/azuredeveloper-files:latest as filecontainer

FROM microsoft/powershell


# RUN pip install -r requirements.txt 

# RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
# RUN apt-key add microsoft.asc
# RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | tee /etc/apt/sources.list.d/microsoft.list

# WORKDIR /powershell-install
RUN apt-get update
RUN apt-get install -y jq wget

WORKDIR /app
COPY . /app

RUN pwsh -c "Install-Module Az -Force"
RUN pwsh -c "Import-Module Az -Force"

# RUN curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.03.1-ce.tgz && tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin

# WORKDIR /files
# COPY --from=filecontainer /files .
# RUN tar --strip-components=1 -xvzf docker-18.06.1-ce.tgz -C /usr/local/bin
# RUN rm *

ENTRYPOINT tail -f /dev/null
