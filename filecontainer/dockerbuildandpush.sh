#!/bin/sh

cat /run/secrets/dockerloginpassword | docker login --username geircode --password-stdin
cd /app
docker build -f Dockerfile -t geircode/azuredeveloper:latest .
docker push geircode/azuredeveloper:latest