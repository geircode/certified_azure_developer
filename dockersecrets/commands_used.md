# Commands used in this setup

## Most important commands

Start Container by running: *docker-compose.dockersecrets.up.bat*

```shell
ssh-keygen -t rsa -b 4096 -C your@email.no -P MyPassword -f /azuredeveloper_ef3a8e3_secrets/ubuntu_rsa
```