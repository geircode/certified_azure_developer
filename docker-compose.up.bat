cd %~dp0
docker rm -f azuredeveloper_ef3a8e3-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
REM wait for 1-2 seconds for the container to start
pause
docker exec -it azuredeveloper_ef3a8e3-1 /bin/bash